#include "tp1.h"
#include <QApplication>
#include <time.h>

void exprcarre(Point * z);

int isMandelbrot(Point z, int n, Point point)
{
    if(n >= 0)
    {                                          // on fait la récursion n fois, condition d'arret n=0
        exprcarre(&z);                               // on met l'expression au carré 
        z.x += point.x;                                   // z^2 + point
        z.y += point.y;

        if(sqrt(pow(z.x,2) + pow(z.y,2)) > 2)           // point appartient à l'ensemble
        {          
            return 1;
        }
        
        else
        { 
            return isMandelbrot(z,n-1,point);           // point appartient pas à l'ensemble, on test le rang inférieur
        }
    }
    
    else
    {
        return 0;
    }
}

void exprcarre(Point * z){ // Fonction pour mettre l'expression au carré via identité remarquable
    z->x = pow(z->x, 2) - pow(z->y, 2); // x^2 -y^2 au carré comme i^2 = -1
    z->y = 2*z->x*z->y; // 2*x*y
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



