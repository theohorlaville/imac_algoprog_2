#include <iostream> // les commentaires serviront pour mes révisions, ne pas y préter attention

using namespace std;


struct Noeud{              
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int nbNoeud;
};

struct DynaTableau{
    int* donnees;
    int nbDonnees;
    int capacite;
};

void initialise(Liste* liste)
{
    liste->premier = NULL;             // On crée une liste vide
    liste->nbNoeud = 0;
}

bool est_vide(const Liste* liste) 
{
    if(liste->nbNoeud == 0){        // Vérification si elle est vide ou non
        return true;
    }else{
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    //On créer le nouveau noeud et on l'initialise
    Noeud * nvNoeud = new Noeud;
    nvNoeud->donnee = valeur;                              // On crée le noeud qui va être utilisé dans la liste
    nvNoeud->suivant = NULL;

    //On parcours la liste pour trouver le dernier noeud
    if(liste->premier == NULL)
    {
        liste->premier = nvNoeud;
    }
    else
    {
        nvNoeud->suivant = liste->premier;
        liste->premier=nvNoeud;

    }

    liste->nbNoeud += 1;
}

void affiche(const Liste* liste)
{
   
    cout << "LISTE:" << endl;


    int count = 1;
    Noeud * curseur = liste->premier;              // On créé un curseur que l'on met à la premiere place 


    while(curseur != NULL){
        cout << "Valeur " << count << " : " << curseur->donnee << endl;
        curseur = curseur->suivant;
        count++;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    if(n>=0 && n<=liste->nbNoeud){         // On nous envoit un entier, on doit renvoyer le nieme noeud correspondant
        Noeud * curseur = liste->premier;      // On créé un curseur

        for(int i=0; i<n; i++){          //On parcours notre liste en nous arretant à la valeur demandée
            curseur = curseur->suivant;
        }

        return curseur->donnee;                //On renvoie la valeur de noeud n
    }else{
        return -1;
    }
}

int cherche(const Liste* liste, int valeur)
{
    int count = 1;
    int index = -1;
    bool trouve = false;

    Noeud * curseur = liste->premier;  // on créé un curseur

    while(!trouve && curseur != NULL){  // tant que la valeur n'est pas trouvé et on est pas arrivé à la fin de la liste on continue de la parcourir
        if(curseur->donnee == valeur){  //Si on trouve la valeur on enregistre le numéro de noeud dans index pour le renvoyer
            index = count;
            trouve = true;
        }
        curseur = curseur->suivant;
        count++;
    }

    return index;      // renvoie -1 si la valeur n'a pas été trouvée
}

void stocke(Liste* liste, int n, int valeur)      // on nous donne un rang n de noeud auquel on doit changer sa valeur
{
    if(n>=0 && n<=liste->nbNoeud){              // Si le rang demandé est inférieur à 0 ou si il dépasse la liste renvoie une erreur
        Noeud * curseur = liste->premier;           // on créé un curseur

        for(int i=0; i<n-1; i++){               //on se positionne au n noeud
            curseur = curseur->suivant;
        }

        curseur->donnee = valeur;                   // on modifie sa valeur
    }else{
        cout << "Rang pas dans la liste" << endl;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;               // on rentre la capacité du tableau qu'on nous donne
    tableau->donnees = new int[capacite];       // on alloue la mémoire nécessaire pour les données selon la capacité envoyée
    tableau->nbDonnees = 0;                     // de base pas de donnée
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->nbDonnees == tableau->capacite){        // si jamais on le tableu est plein
        int * curseur = new int[tableau->capacite + 1];     // 

        for(int i=0; i<tableau->capacite; i++){
            curseur[i] = tableau->donnees[i];
        }
        curseur[tableau->capacite] = valeur;

        free(tableau->donnees);

        tableau->donnees = curseur;
        tableau->capacite = tableau->capacite + 1;
    }else{                                              // on se met à la fin du tableau est on ajoute la valeur
        tableau->donnees[tableau->nbDonnees] = valeur;
    }
    tableau->nbDonnees += 1;        // incrémentation du nbdedonnées
}


bool est_vide(const DynaTableau* tableau)
{
    if(tableau->nbDonnees == 0){                 // verification si le tableau est vide
        return true; 
    }else{
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    cout << endl;
    cout << "TABLEAU DYNAMIQUE :" << endl;
    cout << endl;
    for(int i=0; i<tableau->nbDonnees; i++){
        cout << "Valeur "<< i+1 << " : " << tableau->donnees[i] << endl;     // on parcours le tableau normalement avec une incrémentation
    }
    cout << endl;
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>=0 && n <tableau->nbDonnees){                       // verification du rang donné
        return tableau->donnees[n];                          // renvoi la valeur au n rang dans le tableau
    }else{
        cout << "Rang pas dans le tableau" << endl;
        return -1;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int count = 0, index = -1;
    bool trouve = false;

    while(!trouve && count < tableau->nbDonnees){  // on parcours le tableau
        if(tableau->donnees[count] == valeur){     // si on trouve une valeur corespondant, on renvoie l'index du tableau
            index = count+1;
            trouve = true;
        }
        count++;
    }

    return index;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if(n>=0 && n <tableau->nbDonnees){             // on change tout simplement la valeur au rang n indiqué 
         tableau->donnees[n-1] = valeur;
    }else{
        cout << "Rang pas dans le tableau" << endl;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);          // on ajoute une nouvelle valeur à la liste
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud * curseur = liste->premier;     //On se place au premier noeud
    int valeur = curseur->donnee;         //On extrait sa valeur

    liste->premier = liste->premier->suivant; //Le premier noeud est maintenant le second
    liste->nbNoeud = liste->nbNoeud - 1; //ON enleve un noeud au compteur
    free(curseur);          

    return valeur; //renvoie la valeur extraite
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud * nvNoeud = new Noeud; //on crée un noeud

    nvNoeud->donnee = valeur;     //on initialise sa valeur avec celle envoyée
    nvNoeud->suivant = liste->premier;  //il pointe sur le premier noeud de la liste
    liste->premier = nvNoeud; //la liste pointe vers lui, il devient le premier noeud
    liste->nbNoeud = liste->nbNoeud + 1; // on incrémente de 1 le nombre de noeud
 }

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud * curseur = liste->premier;  //curseur pointe vers le premier noeud
    int valeur = curseur->donnee;       // on récupere sa valeur

    liste->premier = liste->premier->suivant; //  on le supprime comme pour la file;
    liste->nbNoeud = liste->nbNoeud - 1;
    free(curseur);

    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
