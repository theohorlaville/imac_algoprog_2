#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)

    Array& first = w->newArray(origin.size()/2);
    Array& second = w->newArray(origin.size()-first.size());

    // split
    for(int i=0; i<origin.size(); i++) // on sépare le tableau en moitié, en deux sous tableaux
    {
        if(i<origin.size()/2)
        {
            first.set(i, origin.get(i));
        }

        else
        {
            second.set(i-origin.size()/2, origin.get(i));
        }
    }

    // recursiv splitAndMerge of lowerArray and greaterArray

    if(origin.size()/2 > 1)           // on divise récursivement les deux tableaux pour qu'au final on n'est plus qu'une valeur par tableau
    {
        splitAndMerge(first);
    }

    if(origin.size()-first.size() > 1)
    {
        splitAndMerge(second);
    }

    // merge
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    int limitFirst = 0;
    int limitSecond = 0;


    for(int i=0; i<result.size(); i++)
    {
        if(limitFirst == first.size() || limitSecond == second.size())
        {
            if(limitFirst == first.size())
            {
                result.set(i,second[limitSecond]);
                limitSecond++;
            }

            else
            {
                result.set(i,first[limitFirst]);
                limitFirst++;
            }
        }

        else
        {
            if(first[limitFirst] <= second[limitSecond])
            {
                result.set(i,first[limitFirst]);
                limitFirst++;
            }
            else
            {
                result.set(i,second[limitSecond]);
                limitSecond++;
            }
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
    w->show();

    return a.exec();
}
