#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());

    sorted.get(0)=toSort.get(0);
    for(int i=1; i<toSort.size();i++){

        int min=toSort.get(i);
        int index=-1;

        for(int j=0;j<i;j++)
        {
            if(sorted.get(j)>min)
            {
                sorted.insert(j,min);
                index=1;
                break;
            }

        }

        if(index==-1)
        {
            sorted.get(i)=min;
        }

    }
	
    toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
