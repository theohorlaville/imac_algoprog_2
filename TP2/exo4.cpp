#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

// note pour moi : l'algorithme va d'abord appeler récursivement le recursiveQuickSort sur le premier lowerArray, puis va le merge, ensuite il va
// appeler récursivement le recursiveQuickSort sur le premier GreaterArray puis va le merge
// Enfin il va merge les deux tableaux.

void recursivQuickSort(Array& toSort, int size)
{

    Array& lowerArray = w->newArray(size);
    Array& greaterArray= w->newArray(size);
    int pivot;
    int lowerSize = 0;
    int greaterSize = 0;


    if(size > 1)
    {
        pivot = toSort[0]; // pivot première valeur du tableau
        // split
        for(int i=1; i<size; i++) // on sépare les nombres dans deux tableaux selon si ils sont supérieurs ou inférieur au pivot (ici la première valeur)
        {
            if(toSort[i] >= pivot)
            {
                greaterArray.set(greaterSize,toSort[i]);
                greaterSize++;
            }
            else
            {
                lowerArray.set(lowerSize,toSort[i]);
                lowerSize++;
            }
        }

        lowerArray.set(lowerSize, pivot);// on met le pivot à la fin du tableau inférieur, de cette manière il sera trié
        lowerSize++;

        // recursiv sort of lowerArray and greaterArray // récursion pour trier les sous tableaux < et > du pivot
        recursivQuickSort(lowerArray, lowerSize); // d'abord on tri le tableau inférieur ( condition d'arret : le chaque chiffre est trié et est dans un tableau de 1)
        recursivQuickSort(greaterArray, greaterSize); // ensuite le tableau supérieur ( condition d'arret : le chaque chiffre est trié et est dans un tableau de 1)

        // merge
        Array& tabTrie = w->newArray(size); // on créé un tableau qui va récupérer les sous parties avec la taille initiale

        for(int i=0; i<greaterSize; i++) // on remplit la deuxième partie du tableau avec les valeurs supérieures au pivot initial
        {
            tabTrie.set(lowerSize + i,greaterArray[i]);
        }

        for(int i=0; i<lowerSize; i++) // on remplit la première partie du tableau avec les valeurs inferieures au pivot initial + le pivot
        {
            tabTrie.set(i,lowerArray[i]);
        }


        toSort = tabTrie;
    }

}
void quickSort(Array& toSort)
{
    recursivQuickSort(toSort, toSort.size());
}
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
