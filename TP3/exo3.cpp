#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        this->left=NULL;
        this->right=NULL;
        this->value=value;
    }

    void insertNumber(int value) //create a new node and insert it in right or left child
    {
        Node * nouveauNoeud=createNode(value);

        if(this->value<value)
        {
                if(this->right==NULL)
                {
                   this->right=nouveauNoeud;                }

                else
                {
                   this->right->insertNumber(value);
                }
        }

        else
        {
            if(this->left==NULL)
            {
                this->left=nouveauNoeud;
            }

            else
            {
                this->left->insertNumber(value);
            }
        }
    }

        uint height() const{

        uint hauteurDroite=0;
        uint hauteurGauche=0;

        if(isLeaf()) return 1;

        if(this->left!=NULL)
        {
            hauteurGauche=this->left->height();
        }

        if(this->right!=NULL)
        {
            hauteurDroite=this->right->height();
        }

        if(hauteurGauche>hauteurDroite)return hauteurGauche+1;
        else return hauteurDroite+1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 10
        if(isLeaf())return 1;


        int compteurGauche=0;
        int compteurDroite=0;

        if(this->left!=NULL)
        {
            compteurGauche=this->left->nodesCount();
        }
        if(this->right!=NULL){
            compteurDroite=this->right->nodesCount();
        }
        return compteurDroite+compteurGauche+1;

	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->left==NULL && this->right==NULL) return true;
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(isLeaf())
        {
            leaves[leavesCount]=this;
            leavesCount++;
        }

        if(this->left!=NULL)
        {
            this->left->allLeaves(leaves,leavesCount);
        }
        if(this->right!=NULL)
        {
            this->right->allLeaves(leaves,leavesCount);
        }
	}

    void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->left!=NULL)
        {
            this->left->inorderTravel(nodes,nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount++;

        if(this->right!=NULL)
        {
            this->right->inorderTravel(nodes,nodesCount);
        }
    }

    void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this;
        nodesCount++;

        if(this->left!=NULL)
        {
            this->left->preorderTravel(nodes,nodesCount);
        }

        if(this->right!=NULL)
        {
            this->right->preorderTravel(nodes,nodesCount);
        }
    }

    void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if(this->left!=NULL)
        {
            this->left->postorderTravel(nodes,nodesCount);
        }

        if(this->right!=NULL)
        {
            this->right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;

    }

	Node* find(int value) {
        if(this->value==value)
          {
              return this;
          }

          else
         {
              if(value>=this->value && this->right!=NULL)
              {
                  this->right->find(value);
              }

              else if(this->left!=NULL)
              {
                  this->left->find(value);
              }
         }
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
