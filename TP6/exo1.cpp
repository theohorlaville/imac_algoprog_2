#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
    // creation des nodes
    for(int i=0; i<nodeCount;i++)
    {
        GraphNode * node =new GraphNode(i);
        this->appendNewNode(node);
    }
    // création des edges
    for(int j=0; j<nodeCount;j++)
    {
        for(int k=0; k<nodeCount;k++)
        {
            if(adjacencies[j][k]!=0)
            {
                this->nodes[j]->appendNewEdge(nodes[k],adjacencies[j][k]);
            }
        }
    }
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
    */
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
     nodes[nodesSize]=first;
     nodesSize++;
     visited[first->value]=true;
     Edge * crt=first->edges;

     while(crt!=NULL)
     {
         if(!visited[crt->destination->value])
         {
             this->deepTravel(crt->destination, nodes, nodesSize, visited);
         }

         crt=crt->next;
     }

    /*
    visited[]
    nodeSize++;
    this->deepTravel(,nodes,*nodesSize,visited);*/
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{

	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);

    while(!nodeQueue.empty())
    {
        GraphNode * node;
        node=nodeQueue.front();
        nodeQueue.pop();
        visited[node->value]=true;
        nodes[nodesSize]=node;
        nodesSize++;

        for(Edge *edge=node->edges;edge!=NULL;edge=edge->next)
        {
            if(!visited[edge->destination->value])
            {
                nodeQueue.push(edge->destination);
            }
        }
    }
    /**
     * Fill nodes array by travelling graph starting from first and using queue
     * nodeQueue.push(a_node)
     * nodeQueue.front() -> first node of the queue
     * nodeQueue.pop() -> remove first node of the queue
     * nodeQueue.size() -> size of the queue
     */
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
    // PAS ENCORE FAIT


	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
